<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTablas([
            'users',
            'publications',
            'comments'         
        ]);
        $this->call(UserSeeder::class);
        $this->call(PublicationSeeder::class);
        $this->call(CommentSeeder::class);
    }

    protected function truncateTablas(array $tablas)
    {
        Schema::disableForeignKeyConstraints(0);
        foreach ($tablas as $tabla) {
            DB::table($tabla)->truncate();
        }
        Schema::disableForeignKeyConstraints(1);
    }
}
