<?php

use App\Publication;
use Illuminate\Database\Seeder;

class PublicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Publication::class, 15)->create();
        factory(Publication::class, 6)->create(['user_id' => 1]);
    }
}
