<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Oliver Olivares',
                'email' => 'olivares.oliver13@gmail.com',
                'password' => '$2y$10$XP2B7hmxAKrUoDxO.0zwg.16I1NPEE6Y34fXTb4Em43MaJp4FImCa',
                'created_at' => date('Y-m-d H:m:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ],
        ]); 
    }
}
