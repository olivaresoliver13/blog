@extends('layouts.app')

@section('content')
    <div role="main" class="container">
        <h1 class="mt-5">Crear nueva publicación</h1>
        <form action="{{ route('publications.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Título</label>
                <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" id="title" aria-describedby="title" placeholder="Enter title">
            </div>
            <div class="form-group">
                <label for="content">Contenido</label>
                <textarea type="content" name="content" class="form-control @error('content') is-invalid @enderror" id="content" placeholder="Enter the content" rows="15"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
    </div>
@endsection